/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.633983286908078, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.985207100591716, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.94, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.9655172413793104, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.69, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.011695906432748537, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.9900990099009901, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.8419540229885057, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.8600917431192661, 500, 1500, "bankAccountsByFkChild"], "isController": false}, {"data": [0.9775784753363229, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.5148148148148148, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.8856502242152466, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.9653465346534653, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.96, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.68, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.7909090909090909, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.0028735632183908046, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.0, 500, 1500, "getChildStatementOfAccount"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.9663677130044843, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.984304932735426, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.28867924528301886, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.6153846153846154, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}, {"data": [0.77, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3590, 0, 0.0, 1682.868245125348, 9, 28457, 441.5, 4766.400000000001, 6457.649999999997, 24180.94000000001, 11.681520745274517, 239.88169803736625, 20.58239175985039], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllFeeGroup", 169, 0, 0.0, 189.34319526627212, 12, 659, 159.0, 344.0, 386.0, 627.5000000000005, 0.5869074492099323, 0.5582498589164786, 0.6327595936794582], "isController": false}, {"data": ["findAllChildActiveSubsidies", 50, 0, 0.0, 288.4599999999999, 34, 627, 233.0, 540.8, 617.5, 627.0, 0.1946381093632609, 0.21888423947105148, 0.3174273853873493], "isController": false}, {"data": ["findAllProgramBillingUpload", 174, 0, 0.0, 253.09770114942523, 19, 889, 238.0, 469.5, 540.25, 863.5, 0.585777720920681, 0.7877009981366209, 0.8740901929363286], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 50, 0, 0.0, 586.9599999999999, 268, 1102, 554.5, 989.5999999999999, 1021.1999999999998, 1102.0, 0.19479355778745686, 0.28318493919519094, 0.35040013031689016], "isController": false}, {"data": ["findAllCurrentFeeTier", 171, 0, 0.0, 2293.5906432748534, 1299, 3617, 2318.0, 2852.6, 3099.0000000000005, 3575.2400000000002, 0.5740373963543589, 4.233079954009869, 1.4535927429168485], "isController": false}, {"data": ["findAllWithdrawalDraft", 101, 0, 0.0, 150.59405940594056, 13, 913, 135.0, 294.19999999999993, 363.3999999999995, 907.3600000000012, 0.34345096812366954, 0.17340249074212613, 1.1142032383855764], "isController": false}, {"data": ["listBulkInvoiceRequest", 174, 0, 0.0, 408.61494252873575, 48, 892, 420.0, 653.0, 711.25, 826.0, 0.5843671706553645, 0.9011584858761142, 0.8856814930245368], "isController": false}, {"data": ["bankAccountsByFkChild", 218, 0, 0.0, 380.07798165137626, 36, 1439, 312.5, 822.6999999999999, 1021.9499999999997, 1346.7900000000002, 0.7512008876575627, 1.0767721976847162, 1.7290825119227298], "isController": false}, {"data": ["findAllCustomSubsidies", 223, 0, 0.0, 196.00448430493282, 30, 779, 172.0, 402.99999999999994, 473.39999999999975, 734.9999999999993, 0.7476923799081981, 0.8966041553506946, 0.9353456432250018], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 270, 0, 0.0, 946.0518518518514, 364, 1629, 955.0, 1330.5, 1420.0, 1549.0, 0.907797984688474, 80.35755985057477, 1.3085056888673707], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 223, 0, 0.0, 295.45291479820617, 18, 1149, 247.0, 663.4, 749.5999999999999, 1098.3199999999997, 0.7491567786930405, 0.5391880331023153, 0.9832682720346158], "isController": false}, {"data": ["getTransferDrafts", 101, 0, 0.0, 207.6435643564356, 11, 815, 186.0, 403.59999999999997, 560.0, 813.2000000000004, 0.34351755170619386, 0.17175877585309693, 0.5890789265586683], "isController": false}, {"data": ["getAllChildDiscounts", 50, 0, 0.0, 262.52, 17, 720, 245.5, 473.9, 595.7999999999996, 720.0, 0.1931874381800198, 0.11810091435614492, 0.3184574176248764], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 50, 0, 0.0, 729.0, 31, 2805, 562.0, 1251.8, 2347.599999999997, 2805.0, 0.1927161022012033, 0.1550762384900308, 0.2567038705101966], "isController": false}, {"data": ["invoicesByFkChild", 55, 0, 0.0, 499.6545454545455, 132, 1122, 423.0, 977.1999999999999, 1105.6, 1122.0, 0.19273359311485522, 0.5503687070291694, 0.5603202213895742], "isController": false}, {"data": ["findAllFeeDraft", 174, 0, 0.0, 3361.3218390804586, 1488, 4720, 3436.0, 4299.5, 4404.5, 4624.75, 0.5789964062292028, 0.3290780355716758, 0.9227755224277919], "isController": false}, {"data": ["getChildStatementOfAccount", 223, 0, 0.0, 5511.130044843049, 3495, 7394, 5432.0, 6655.4, 6899.799999999999, 7387.32, 0.7405218188278503, 1.1018226062134098, 1.207686950627451], "isController": false}, {"data": ["findAllConsolidatedRefund", 96, 0, 0.0, 4349.28125, 2379, 5495, 4434.0, 4968.8, 5273.449999999999, 5495.0, 0.33882273218180664, 0.46911424546648123, 0.585660386681443], "isController": false}, {"data": ["getAdvancePaymentReceipts", 223, 0, 0.0, 206.75784753363232, 13, 747, 189.0, 451.4, 558.5999999999999, 665.1199999999999, 0.7490611542931617, 1.1265177515737002, 1.1952792247217052], "isController": false}, {"data": ["getRefundChildBalance", 223, 0, 0.0, 149.08968609865474, 9, 707, 117.0, 342.6, 443.1999999999997, 617.76, 0.749176913256736, 0.7147908635271786, 1.1735153992810587], "isController": false}, {"data": ["findAllUploadedGiroFiles", 265, 0, 0.0, 1426.003773584906, 671, 2356, 1409.0, 1837.8, 1913.7, 2174.9999999999986, 0.9159155007465575, 155.67937883755636, 1.3112618399359897], "isController": false}, {"data": ["findAllInvoice", 156, 0, 0.0, 9034.60897435897, 27, 28457, 365.5, 26553.4, 27418.8, 28427.36, 0.5089241148472086, 0.9103250120706361, 1.6323298784535885], "isController": false}, {"data": ["findAllCreditDebitNotes", 101, 0, 0.0, 7673.603960396039, 3967, 9546, 7718.0, 8701.2, 9401.599999999997, 9544.68, 0.3340079632789661, 0.6368350895620859, 0.7805479063735995], "isController": false}, {"data": ["bankAccountInfoByIDChild", 50, 0, 0.0, 483.98, 101, 1369, 462.5, 844.9999999999998, 1074.9999999999993, 1369.0, 0.1931240126534853, 0.2800449061610422, 0.44471330257511554], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3590, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
